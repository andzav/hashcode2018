#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

struct vehicle {
	int x = 0;
	int y = 0;
	bool taken = false;
	long takenUntil;
	vector<int> ridesDone;
};

struct ride {
	int id=0;
	int startx=0;
	int starty=0;
	int endx=0;
	int endy=0;
	int earliest_start=0;
	int latest_finish=0;
	bool finished = false;
};

int distanse_between_points(int x1, int x2, int y1, int y2) {
	return abs(x2 - x1) + abs(y2 - y1);
}

bool compareByStart(const ride &a, const ride &b)
{
	return a.earliest_start < b.earliest_start;
}

bool compareByLength(const ride &a, const ride &b)
{
	return distanse_between_points(a.startx, a.starty, a.endx, a.endy) < distanse_between_points(b.startx, b.starty, b.endx, b.endy);
}

void printArr(vector<ride> arr, int size) {
	for (int i = 0; i < size; i++)
		cout<< arr[i].finished << endl;
}

void printResult(vehicle *arr, int size) {
	for (int i = 0; i < size; i++) {
		cout << arr[i].ridesDone.size() << " ";
		for (int j = 0; j < arr[i].ridesDone.size(); j++)
			cout << arr[i].ridesDone[j] << " ";
		cout << endl;
	}
}

int main() {
	ifstream f("3.in");
	int rows, columns, vehicles, rides, bonus, steps, tick, finished = 0;
	f >> rows >> columns >> vehicles >> rides >> bonus >> steps;
	vector<ride> ridesDB;
	vehicle *vehiclesDB = new vehicle[vehicles];

	for (int i = 0; i < rides; ++i) {
		ride temp;
		f >> temp.startx >> temp.starty >> temp.endx >> temp.endy >> temp.earliest_start >> temp.latest_finish;
		temp.id = i;
		ridesDB.push_back(temp);
	}
	sort(ridesDB.begin(), ridesDB.end(), compareByLength);
	ridesDB.erase(ridesDB.end() - (int)ridesDB.size()*0.3, ridesDB.end());
	sort(ridesDB.begin(), ridesDB.end(), compareByStart);

	for (int i = 0; i < vehicles; ++i) {
		vehiclesDB[i].ridesDone.push_back(ridesDB[i].id);
		int dist = distanse_between_points(vehiclesDB[i].x, vehiclesDB[i].y, ridesDB[i].startx, ridesDB[i].starty);
		int delay = ridesDB[i].earliest_start - dist > 0 ? ridesDB[i].earliest_start - dist : 0;
		vehiclesDB[i].takenUntil = dist + delay + distanse_between_points(ridesDB[i].startx, ridesDB[i].starty, ridesDB[i].endx, ridesDB[i].endy);
		vehiclesDB[i].x = ridesDB[i].endx;
		vehiclesDB[i].y = ridesDB[i].endy;
		ridesDB[i].finished = true;
		++finished;
	}

	for (tick = 0; (tick < steps) && (finished < ridesDB.size()); ++tick) {
		for (int k = 0; k < ridesDB.size(); ++k) {
			if (ridesDB[k].finished == false) {
				int arriveTick, selectedID = -1;
				for (int i = 0; i < vehicles; ++i) {
					if (vehiclesDB[i].takenUntil == tick) vehiclesDB[i].taken = false;
					int modifier = vehiclesDB[i].taken ? (vehiclesDB[i].takenUntil - tick) : 0;
					int timeToArrive = modifier + distanse_between_points(vehiclesDB[i].x, vehiclesDB[i].y, ridesDB[k].startx, ridesDB[k].starty);
					if (selectedID == -1) {
						selectedID = i;
						arriveTick = timeToArrive;
					}
					else if (selectedID != -1 && timeToArrive < arriveTick) {
						arriveTick = timeToArrive;
						selectedID = i;
					}
				}
				if (selectedID != -1) {
					ridesDB[k].finished = true;
					++finished;
					int delay = ridesDB[k].earliest_start - tick - arriveTick > 0 ? ridesDB[k].earliest_start - tick - arriveTick : 0;
					vehiclesDB[selectedID].takenUntil = delay + tick + arriveTick + distanse_between_points(ridesDB[k].startx, ridesDB[k].starty, ridesDB[k].endx, ridesDB[k].endy);
					vehiclesDB[selectedID].taken = true;
					vehiclesDB[selectedID].x = ridesDB[k].endx;
					vehiclesDB[selectedID].y = ridesDB[k].endy;
					vehiclesDB[selectedID].ridesDone.push_back(ridesDB[k].id);
				}
			}
		}	
	}
	ofstream out("3.out");
	for (int i = 0; i < vehicles; ++i)
	{
		out << vehiclesDB[i].ridesDone.size() << " ";
		for (int j = 0; j < vehiclesDB[i].ridesDone.size(); j++)
			out << vehiclesDB[i].ridesDone[j] << " ";
		out << "\n";
	}
	cout << "Done..." << endl;
	cin.get();
	return 0;
}